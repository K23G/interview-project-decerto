FROM openjdk:11-jdk

ARG JAR_FILE=target/numbers-0.0.1-SNAPSHOT.jar
ARG CONFIG=src/main/resources

ADD ${JAR_FILE} app.jar
ADD ${CONFIG} src/main/resources

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]