# Decerto Interview

Application created for interview in Decerto \
Author: Krystian Głuszek

## Application purpose

Conducting operations on numeric data from provided sources.

## How to build

To build the application you should run commands in the following order:

1. Building application

`mvn clean package`

2. Starting the application

`mvn spring-boot:run`

## After start

### From web browser

At http://localhost:8080/swagger-ui/ you can access swagger documentation.

Swagger can also be used as a service to test the application from a browser. For that you need to go to \
http://localhost:8080/swagger-ui/#/calculate-controller/calculateUsingPOST and then, using example request body:

`{
"operation": "ADD_INTEGERS",
"providers": [
"CORE_JAVA", "RANDOM_ORG_API"
]
}`

You will get te result of adding two random integers from core java random library and random.org api's response.

### From Command Line

You can send those example requests:

1. For CORE_JAVA and DATABASE:

`curl -X POST "http://localhost:8080/calculate" -H "accept: */*" -H "Content-Type: application/json" -d "{ "operation": "ADD_INTEGERS", "providers": [ "CORE_JAVA", "DATABASE"]}"`

2. For CORE_JAVA and RANDOM_ORG_API

`curl -X POST "http://localhost:8080/calculate" -H "accept: */*" -H "Content-Type: application/json" -d "{ "operation": ", "providers": [ "CORE_JAVA", "RANDOM_ORG_API" ]}"`

3. For DATABASE and RANDOM_ORG_API:

`curl -X POST "http://localhost:8080/calculate" -H "accept: */*" -H "Content-Type: application/json" -d "{ /"operation": ", "providers": [ "DATABASE", "RANDOM_ORG_API" ]}"`

4. To get OperationNotFoundException:

`curl -X POST "http://localhost:8080/calculate" -H "accept: */*" -H "Content-Type: application/json" -d "{ "operation": "MULTIPLY_INTEGERS", "providers": [ "CORE_JAVA", "DATABASE" ]}"
`