package com.decerto.numbers.configuration.exception;

import com.decerto.numbers.model.enums.OperationEnum;

public class OperationNotFoundException extends RuntimeException {

    public OperationNotFoundException(OperationEnum operation) {
        super("Operation: " + operation.name() + " not found!");
    }
}
