package com.decerto.numbers.configuration.exception;

public class RandomOrgException extends RuntimeException {

    public RandomOrgException(String errorMessage) {
        super("Random.org exception with message: " + errorMessage);
    }
}
