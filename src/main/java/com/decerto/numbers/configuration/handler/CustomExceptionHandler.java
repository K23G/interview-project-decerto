package com.decerto.numbers.configuration.handler;

import com.decerto.numbers.configuration.exception.OperationNotFoundException;
import com.decerto.numbers.configuration.exception.RandomOrgException;
import com.decerto.numbers.model.dto.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(OperationNotFoundException.class)
    protected ResponseEntity<ErrorResponse> handleOperationNotFound(OperationNotFoundException ex) {
        return wrap(new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage()));
    }

    @ExceptionHandler(RandomOrgException.class)
    protected ResponseEntity<ErrorResponse> handleRandomOrgException(RandomOrgException ex) {
        return wrap(new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage()));
    }

    private ResponseEntity<ErrorResponse> wrap(ErrorResponse errorResponse) {
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }
}
