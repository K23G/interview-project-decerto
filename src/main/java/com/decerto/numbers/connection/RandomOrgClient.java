package com.decerto.numbers.connection;

import com.decerto.numbers.model.dto.random.request.RandomOrgRequest;
import com.decerto.numbers.model.dto.random.response.RandomOrgResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "RandomOrgClient", url = "${org.random.api.url}")
public interface RandomOrgClient {

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    RandomOrgResponse sendPost(RandomOrgRequest request);
}
