package com.decerto.numbers.connection.service;

import com.decerto.numbers.configuration.exception.RandomOrgException;
import com.decerto.numbers.connection.RandomOrgClient;
import com.decerto.numbers.model.dto.random.request.RandomOrgRequest;
import com.decerto.numbers.model.dto.random.request.RandomOrgRequestParams;
import com.decerto.numbers.model.dto.random.response.RandomOrgResponse;
import com.decerto.numbers.model.dto.random.response.error.RandomOrgResponseError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RandomOrgClientService {

    private static final Integer ID = 1;
    private static final Integer MIN = 0;
    private static final Integer MAX = 100;
    private static final Integer N = 100;
    private static final String METHOD_NAME = "generateIntegers";

    @Value("${org.random.api.key}")
    private String apiKey;
    @Value("${org.random.api.jsonrpc.version}")
    private String jsonRpc;

    private final RandomOrgClient randomOrgClient;

    public RandomOrgClientService(RandomOrgClient randomOrgClient) {
        this.randomOrgClient = randomOrgClient;
    }

    public Integer getGeneratedInteger() {
        RandomOrgResponse response = randomOrgClient.sendPost(buildRequest());

        if (response.getError() != null) {
            RandomOrgResponseError error = response.getError();
            log.warn("Error while collecting data from RandomOrg. Code {} and message {}",
                    error.getCode(), error.getMessage());
            throw new RandomOrgException(error.getMessage());
        }

        return response.getResult().getRandom().getData()
                .stream()
                .findAny()
                .orElse(0);
    }

    private RandomOrgRequest buildRequest() {
        return RandomOrgRequest.builder()
                .id(ID)
                .method(METHOD_NAME)
                .jsonrpc(jsonRpc)
                .params(buildRequestParams())
                .build();
    }

    private RandomOrgRequestParams buildRequestParams() {
        return RandomOrgRequestParams.builder()
                .apiKey(apiKey)
                .min(MIN)
                .max(MAX)
                .n(N)
                .build();
    }
}
