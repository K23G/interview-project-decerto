package com.decerto.numbers.controller;

import com.decerto.numbers.model.dto.request.CalculateRequest;
import com.decerto.numbers.model.dto.response.CalculateResponse;
import com.decerto.numbers.service.CalculateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("calculate")
public class CalculateController {

    private final CalculateService calculateService;

    public CalculateController(CalculateService calculateService) {
        this.calculateService = calculateService;
    }

    @PostMapping
    public ResponseEntity<CalculateResponse> calculate(@RequestBody @Valid CalculateRequest request) {
        return ResponseEntity.ok(calculateService.calculate(request));
    }
}
