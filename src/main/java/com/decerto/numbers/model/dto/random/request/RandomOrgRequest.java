package com.decerto.numbers.model.dto.random.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RandomOrgRequest {

    private String jsonrpc;
    private String method;
    private RandomOrgRequestParams params;
    private Integer id;
}
