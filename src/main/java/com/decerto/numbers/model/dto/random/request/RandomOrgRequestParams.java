package com.decerto.numbers.model.dto.random.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RandomOrgRequestParams {

    private String apiKey;
    private Integer n;
    private Integer min;
    private Integer max;
}
