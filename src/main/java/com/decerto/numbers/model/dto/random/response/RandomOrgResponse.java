package com.decerto.numbers.model.dto.random.response;

import com.decerto.numbers.model.dto.random.response.error.RandomOrgResponseError;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RandomOrgResponse {

    private String jsonrpc;
    private RandomOrgResponseData result;
    private RandomOrgResponseError error;
    private String id;
}
