package com.decerto.numbers.model.dto.random.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class RandomOrgResponseResult {

    private List<Integer> data;
    private String completionTime;
}
