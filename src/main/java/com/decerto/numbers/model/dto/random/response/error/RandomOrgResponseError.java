package com.decerto.numbers.model.dto.random.response.error;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RandomOrgResponseError {

    private String code;
    private String message;
    private String data;
}
