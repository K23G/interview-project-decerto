package com.decerto.numbers.model.dto.request;

import com.decerto.numbers.model.enums.OperationEnum;
import com.decerto.numbers.model.enums.ProviderEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@Builder
public class CalculateRequest {

    @NotNull
    private OperationEnum operation;

    @NotNull
    @Size(min = 2, max = 2)
    private Set<ProviderEnum> providers;
}
