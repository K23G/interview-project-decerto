package com.decerto.numbers.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "INTEGER_NUMBERS")
@Entity(name = "integerNumber")
public class IntegerNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "number_value")
    private Integer value;
}
