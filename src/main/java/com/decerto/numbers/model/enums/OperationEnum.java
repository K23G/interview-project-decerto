package com.decerto.numbers.model.enums;

public enum OperationEnum {

    ADD_INTEGERS,
    ADD_STRINGS,
    /*Multiply integers is not implemented
     * Just to show possible exception*/
    MULTIPLY_INTEGERS
}
