package com.decerto.numbers.model.enums;

public enum ProviderEnum {

    CORE_JAVA,
    DATABASE,
    RANDOM_ORG_API
}
