package com.decerto.numbers.repository;

import com.decerto.numbers.model.entity.IntegerNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegerNumberRepository extends JpaRepository<IntegerNumber, Long> {

}
