package com.decerto.numbers.service;

import com.decerto.numbers.model.dto.request.CalculateRequest;
import com.decerto.numbers.model.dto.response.CalculateResponse;
import org.springframework.stereotype.Service;

@Service
public interface CalculateService {

    CalculateResponse calculate(CalculateRequest request);
}
