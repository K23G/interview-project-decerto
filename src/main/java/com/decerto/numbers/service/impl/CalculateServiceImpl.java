package com.decerto.numbers.service.impl;

import com.decerto.numbers.configuration.exception.OperationNotFoundException;
import com.decerto.numbers.model.dto.request.CalculateRequest;
import com.decerto.numbers.model.dto.response.CalculateResponse;
import com.decerto.numbers.model.enums.OperationEnum;
import com.decerto.numbers.model.enums.ProviderEnum;
import com.decerto.numbers.service.CalculateService;
import com.decerto.numbers.service.operation.OperationService;
import com.decerto.numbers.service.provider.NumberProviderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CalculateServiceImpl implements CalculateService {

    List<OperationService> operationServices;
    List<NumberProviderService> numberProviderServices;

    public CalculateServiceImpl(List<OperationService> operationServices, List<NumberProviderService> numberProviderServices) {
        this.operationServices = operationServices;
        this.numberProviderServices = numberProviderServices;
    }

    @Override
    public CalculateResponse calculate(CalculateRequest request) {
        List<String> values = getValuesForProviders(request.getProviders());

        log.info("Randomized numbers from {} are {}", request.getProviders(), values);

        return CalculateResponse.builder()
                .result(String.valueOf(calculateValuesForOperation(request.getOperation(), values)))
                .build();
    }

    private List<String> getValuesForProviders(Set<ProviderEnum> providers) {
        return numberProviderServices.stream()
                .filter(r -> providers.contains(r.getProvider()))
                .map(NumberProviderService::getValue)
                .collect(Collectors.toList());
    }

    private String calculateValuesForOperation(OperationEnum operation, List<String> values) {
        return operationServices.stream()
                .filter(r -> r.operation().equals(operation))
                .findFirst().orElseThrow(() -> new OperationNotFoundException(operation))
                .calculate(values);
    }
}
