package com.decerto.numbers.service.operation;

import com.decerto.numbers.model.enums.OperationEnum;

import java.util.List;

public interface OperationService {

    OperationEnum operation();

    String calculate(List<String> values);
}
