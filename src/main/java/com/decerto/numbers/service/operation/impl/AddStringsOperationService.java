package com.decerto.numbers.service.operation.impl;

import com.decerto.numbers.model.enums.OperationEnum;
import com.decerto.numbers.service.operation.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class AddStringsOperationService implements OperationService {

    @Override
    public OperationEnum operation() {
        return OperationEnum.ADD_STRINGS;
    }

    @Override
    public String calculate(List<String> values) {
        return String.join("", values);
    }
}
