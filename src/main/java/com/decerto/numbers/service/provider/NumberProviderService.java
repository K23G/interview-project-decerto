package com.decerto.numbers.service.provider;

import com.decerto.numbers.model.enums.ProviderEnum;

public interface NumberProviderService {

    String getValue();

    ProviderEnum getProvider();
}
