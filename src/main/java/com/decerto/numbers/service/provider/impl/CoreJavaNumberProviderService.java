package com.decerto.numbers.service.provider.impl;

import com.decerto.numbers.model.enums.ProviderEnum;
import com.decerto.numbers.service.provider.NumberProviderService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class CoreJavaNumberProviderService implements NumberProviderService {

    @Value("${number.max}")
    private Integer max;

    @Override
    public String getValue() {
        return String.valueOf(new Random().nextInt(max));
    }

    @Override
    public ProviderEnum getProvider() {
        return ProviderEnum.CORE_JAVA;
    }
}
