package com.decerto.numbers.service.provider.impl;

import com.decerto.numbers.model.entity.IntegerNumber;
import com.decerto.numbers.model.enums.ProviderEnum;
import com.decerto.numbers.repository.IntegerNumberRepository;
import com.decerto.numbers.service.provider.NumberProviderService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class DatabaseNumberProviderService implements NumberProviderService {

    private final IntegerNumberRepository integerNumberRepository;

    public DatabaseNumberProviderService(IntegerNumberRepository integerNumberRepository) {
        this.integerNumberRepository = integerNumberRepository;
    }

    @Override
    public String getValue() {
        return findRandom()
                .map(IntegerNumber::getValue)
                .map(String::valueOf)
                .orElse("0");
    }

    @Override
    public ProviderEnum getProvider() {
        return ProviderEnum.DATABASE;
    }

    private Optional<IntegerNumber> findRandom() {
        long numberOfRecords = integerNumberRepository.count();
        return integerNumberRepository.findById(new Random().nextInt() % numberOfRecords);

    }
}
