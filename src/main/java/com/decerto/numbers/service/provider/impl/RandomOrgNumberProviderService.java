package com.decerto.numbers.service.provider.impl;

import com.decerto.numbers.connection.service.RandomOrgClientService;
import com.decerto.numbers.model.enums.ProviderEnum;
import com.decerto.numbers.service.provider.NumberProviderService;
import org.springframework.stereotype.Service;

@Service
public class RandomOrgNumberProviderService implements NumberProviderService {

    private final RandomOrgClientService clientService;

    public RandomOrgNumberProviderService(RandomOrgClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    public String getValue() {
        return String.valueOf(clientService.getGeneratedInteger());
    }

    @Override
    public ProviderEnum getProvider() {
        return ProviderEnum.RANDOM_ORG_API;
    }
}
