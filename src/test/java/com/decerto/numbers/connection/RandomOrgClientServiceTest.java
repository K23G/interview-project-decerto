package com.decerto.numbers.connection;

import com.decerto.numbers.configuration.exception.RandomOrgException;
import com.decerto.numbers.connection.service.RandomOrgClientService;
import com.decerto.numbers.model.dto.random.request.RandomOrgRequest;
import com.decerto.numbers.model.dto.random.response.RandomOrgResponse;
import com.decerto.numbers.model.dto.random.response.RandomOrgResponseData;
import com.decerto.numbers.model.dto.random.response.RandomOrgResponseResult;
import com.decerto.numbers.model.dto.random.response.error.RandomOrgResponseError;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class RandomOrgClientServiceTest {

    private static final Integer RETURNED_NUMBER = 12;

    @Mock
    private RandomOrgClient randomOrgClient;

    @InjectMocks
    private RandomOrgClientService randomOrgClientService;

    @Test
    public void shouldReturnResponseFromRandomOrg() {
        Mockito.when(randomOrgClient.sendPost(any(RandomOrgRequest.class))).thenReturn(createMockSuccessResponse());
        Integer response = randomOrgClientService.getGeneratedInteger();
        log.info("Returned value is {}", response);

        assertEquals(RETURNED_NUMBER, response);
    }

    @Test
    public void shouldThrowExceptionWhenErrorIsPresent() {
        Mockito.when(randomOrgClient.sendPost(any(RandomOrgRequest.class))).thenReturn(createMockErrorResponse());

        assertThrows(RandomOrgException.class, () -> randomOrgClientService.getGeneratedInteger());
    }

    private RandomOrgResponse createMockSuccessResponse() {
        return RandomOrgResponse.builder()
                .id("1")
                .jsonrpc("1")
                .error(null)
                .result(
                        RandomOrgResponseData.builder()
                                .random(
                                        RandomOrgResponseResult.builder()
                                                .completionTime("12:00")
                                                .data(Collections.singletonList(RETURNED_NUMBER)).build()
                                )
                                .bitsLeft(12)
                                .bitsUsed(1)
                                .build()
                )
                .build();
    }

    private RandomOrgResponse createMockErrorResponse() {
        return RandomOrgResponse.builder()
                .id("1")
                .jsonrpc("1")
                .error(
                        RandomOrgResponseError.builder()
                                .code("111")
                                .data("123")
                                .message("Wrong input")
                                .build()
                )
                .result(null)
                .build();
    }
}
