package com.decerto.numbers.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Slf4j
public class IntegerNumberRepositoryTest {

    @Autowired
    private IntegerNumberRepository repository;

    @Test
    public void shouldReturnOnlyOneEntry() {
        Integer integerNumberSize = repository.findAll().size();

        assertEquals(1, integerNumberSize);
    }
}
