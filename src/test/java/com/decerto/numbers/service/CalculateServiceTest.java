package com.decerto.numbers.service;

import com.decerto.numbers.configuration.exception.OperationNotFoundException;
import com.decerto.numbers.model.dto.request.CalculateRequest;
import com.decerto.numbers.model.enums.OperationEnum;
import com.decerto.numbers.model.enums.ProviderEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CalculateServiceTest {

    @Autowired
    private CalculateService calculateService;

    @Test
    public void shouldAddGivenValues() {
        final OperationEnum operation = OperationEnum.ADD_INTEGERS;
        log.info("Testing operation {}", operation.name());

        String unexpectedResult = "";

        String result = calculateService.calculate(buildMockRequest(OperationEnum.ADD_INTEGERS,
                Set.of(ProviderEnum.DATABASE, ProviderEnum.CORE_JAVA))).getResult();

        assertNotEquals(unexpectedResult, result);
    }

    @Test
    public void shouldThrowExceptionForUnimplementedOperation() {
        final OperationEnum operation = OperationEnum.MULTIPLY_INTEGERS;
        log.info("Testing operation {}", operation.name());

        assertThrows(OperationNotFoundException.class,
                () -> calculateService.calculate(buildMockRequest(OperationEnum.MULTIPLY_INTEGERS,
                        Set.of(ProviderEnum.DATABASE, ProviderEnum.CORE_JAVA))));
    }

    private CalculateRequest buildMockRequest(OperationEnum operation, Set<ProviderEnum> providers) {
        return CalculateRequest.builder()
                .operation(operation)
                .providers(providers)
                .build();
    }
}
